# Открываем лог-файл для чтения.

log = open('log_test.txt', 'r')

# Создаем пустые словари rejects и requests.

rejects = {}
requests = {}

# Поочередно обрабатываем каждую строку в лог-файле.
# В каждый момент времени мы будем находиться в одном из трех режимов:
# 1) "find_request_or_reject" - ищем Access-Request'ы и Access-Reject'ы;
# 2) "find_request_params" - ищем ADSL-параметры внутри Access-Request'а;
# 3) "find_reject_request" - ищем, на какой кадр отвечает Access-Reject.
# Начинаем в режиме "find_request_or_reject",

cur_mode = "find_request_or_reject"
cur_frame = None

for cur_line in log:

    # Отрезаем пробелы, переносы и прочий whitespace с обоих концов строки.

    cur_line = cur_line.strip()


    # Режим "find_request_or_reject"

    if cur_mode == "find_request_or_reject":

        # Если в строке есть "Access-Request(1) (id=" ...

        if cur_line.find("Access-Request(1) (id=") != -1:

            # Берем начальную часть строки до первого пробела.
            # Это будет номер кадра.

            cur_frame = int(cur_line.split(" ")[0])

            # Записываем в словарь request элемент с индексом cur_frame, который сам является словарем.
            # В этот словарь нам нужно будет записать два ADSL-параметра, пока их значения - '?'.

            requests[cur_frame] = {'circuit-id': '?', 'remote-id': '?', 'reject-frame': '?'}

            # Переходим в режим "find_request_params".

            cur_mode = "find_request_params"


        # Если в строке есть "Access-Reject(3) (id=" ...

        if cur_line.find("Access-Reject(3) (id=") != -1:

            # Берем начальную часть строки до первого пробела.
            # Это будет номер кадра.

            cur_frame = int(cur_line.split(" ")[0])

            # Записываем в список reject элемент cur_frame.

            rejects[cur_frame] = {'request-frame': '?'}

            # Переходим в режим "find_reject_request".

            cur_mode = "find_reject_request"



    # Режим "find_request_params".

    elif cur_mode == "find_request_params":

        # Если в строке есть "ADSL-Agent-Circuit-Id(1): " ...

        if cur_line.find("ADSL-Agent-Circuit-Id(1): ") != -1:

            # Берем часть строки справа от "ADSL-Agent-Circuit-Id(1): "
            # и записываем ее в requests[frame_id]['circuit-id']

            requests[cur_frame]['circuit-id'] = cur_line.split("ADSL-Agent-Circuit-Id(1): ")[1]


        # Если в строке есть "ADSL-Agent-Remote-Id(2): " ...

        elif cur_line.find("ADSL-Agent-Remote-Id(2): ") != -1:

            # Берем часть строки справа от "ADSL-Agent-Remote-Id(2): "
            # и записываем ее в requests[frame_id]['remote-id']

            requests[cur_frame]['remote-id'] = cur_line.split("ADSL-Agent-Remote-Id(2): ")[1]


        # Если в строке есть "No.     Time        Source" ...

        elif cur_line.find("No.     Time        Source") != -1:

            # Значит, мы дошли до начала нового пакета.
            # Скидываем frame_id в None и переходим обратно в режим "find_request_or_reject".

            frame_id = None
            cur_mode = "find_request_or_reject"



    # Режим "find_reject_request"

    elif cur_mode == "find_reject_request":

        # Если в строке есть "[This is a response to a request in frame " ...

        if cur_line.find("[This is a response to a request in frame ") != -1:

            # Берем часть строки справа от "[This is a response to a request in frame "
            # и до следующего символа ']'. Это номер соответствующего Access-Request.

            req_frame = cur_line.split("[This is a response to a request in frame ")[1]
            req_frame = int(req_frame.split("]")[0])

            # Записываем найденное значение в rejects[frame_id]['request-frame'].

            rejects[cur_frame]['request-frame'] = req_frame

            # Записываем номер текущего кадра в requests[req_frame]['reject-frame'].

            requests[req_frame]['reject-frame'] = cur_frame


        # Если в строке есть "No.     Time        Source" ...

        elif cur_line.find("No.     Time        Source") != -1:

            # Значит, мы дошли до начала нового пакета.
            # Скидываем frame_id в None и переходим обратно в режим "find_request_or_reject".

            frame_id = None
            cur_mode = "find_request_or_reject"


# Закрываем лог-файл.

log.close()

# Выводим информацию о reject'ах и соответствующих им request'ах.

for cur_frame in sorted(rejects):
    if rejects[cur_frame]['request-frame'] not in requests:
        print('Access-Request frame: ?')
        print('Access-Reject frame: %s' % cur_frame)
        print('ADSL-Agent-Circuit-Id: ?')
        print('ADSL-Agent-Remote-Id: ?\n')
    else:
        request_frame = rejects[cur_frame]['request-frame']
        print('Access-Request frame: %s' % request_frame)
        print('Access-Reject frame: %s' % cur_frame)
        print('ADSL-Agent-Circuit-Id: %s' % requests[request_frame]['circuit-id'])
        print('ADSL-Agent-Remote-Id: %s\n' % requests[request_frame]['remote-id'])
